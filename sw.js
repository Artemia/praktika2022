self.addEventListener('push', event => {
    const notification = event.data.text();
    self.registration.showNotification(notification, {});
});

self.addEventListener('install', function(event) {
    console.log('[Service Worker] Installing Service Worker...', event);
    event.waitUntil(
        caches.open('mysite-static-v3').then(function (cache) {
          return cache.addAll([
            '/index.html',
            '/Lemon.html',
            '/tomatoOne.html',
            '/contactUs.html',
            '/js/main.js',
            '/img/Logo.png',
            '/css/contactUs.css',
            '/css/homepage.css',
            '/css/lemon.css',
            '/css/style.css',
            '/css/styleLemon.css',
            '/css/styleOne.css',
            '/css/styleUs.css',
            '/css/tomatoOne.css',
            '/'
          ]);
        }),
      );
});

self.addEventListener('activate', function(event) {
    console.log('[Service Worker] Activating Service Worker...', event);
});

self.addEventListener('fetch', function(event) {
    console.log('[Service Worker] Fetching something...', event);
    event.respondWith(
        caches.match(event.request).then(
            function(response){
                if(response) return response;
                else return fetch(event.request);
            }
        )
    );
});


